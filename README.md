#Prototype
This is a simple example of Prototype, creational design pattern.  

**Description:**  
The prototype pattern is used to instantiate a new object by copying all of the properties of an existing object, creating an independent clone. This practise is particularly useful when the construction of a new object is inefficient.  

![PlantUML model] (http://www.plantuml.com/plantuml/png/SoWkIImgAStDuKfCAYufIamkKKZEIImkLWWeoY_9BwaiI5MevjAkXadM9kVavwLgQ5ef-2TbfIQNSDLoSK7KE-Vd9HUbbgJ2zJc2D1Alt0c7SeGxR6fqTR5OGr254vT3QbuAq8i0)  
