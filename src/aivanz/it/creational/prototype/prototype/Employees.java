package aivanz.it.creational.prototype.prototype;

import java.util.List;

public abstract class Employees {
	public abstract Object clone();

	private List<String> nameList;

	public List<String> getNameList() {
		return nameList;
	}

	public void setNameList(List<String> nameList) {
		this.nameList = nameList;
	}

}
