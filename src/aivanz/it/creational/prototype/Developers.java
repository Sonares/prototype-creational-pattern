package aivanz.it.creational.prototype;

import java.util.LinkedList;
import java.util.List;

import aivanz.it.creational.prototype.prototype.Employees;

public class Developers extends Employees {
	private List<String> preferredLanguages;

	public Developers() {
		preferredLanguages = new LinkedList<>();
	}

	public Developers(List<String> nameList, List<String> preferredLanguages) {
		super.setNameList(nameList);
		this.preferredLanguages = preferredLanguages;
	}

	public List<String> getPreferredLanguages() {
		return this.preferredLanguages;
	}

	public void setPreferredLanguages(List<String> preferredLanguages) {
		this.preferredLanguages = preferredLanguages;
	}

	@Override
	public Object clone() {
		List<String> languages = new LinkedList<>();
		List<String> names = new LinkedList<>();

		for (int i = 0; i < this.preferredLanguages.size(); i++) {
			languages.add(this.preferredLanguages.get(i));
			names.add(super.getNameList().get(i));
		}

		return new Developers(names, languages);
	}

	@Override
	public String toString() {
		return "Developers = " + super.getNameList() + "\nPreferredLanguages = " + this.preferredLanguages;
	}

}
