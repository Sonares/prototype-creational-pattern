package aivanz.it.creational.prototype;

import java.util.LinkedList;
import java.util.List;

/* Creational pattern, it controls class instantiation. 
 * Prototype design pattern is used when the Object creation is a costly affair and requires a lot of time 
 * and resources and you have a similar object already existing. It provides a mechanism to copy the original 
 * object to a new object and then modify it according to our needs. */

/* Example:
 * Developers list. 
 * Suppose we have an Object that loads data from database. Now we need to modify this data in our program 
 * multiple times, so it�s not a good idea to create the Object using new keyword and load all the data again 
 * from database. The better approach would be to clone the existing object into a new object and then do the 
 * data manipulation. */

/* Prototype:
 * +clone(): Object
 * In this case: Employees.
 * This is a "prototype" to the data class. It has the basic attributes and a clone abstract method. 
 * 
 * ConcretePrototype extends Prototype:
 * +clone(): Object
 * In this case: Developers.
 * This is the concrete data class. This class must have a clone method to let copying its values. */

public class MainClass {
	public static void main(String args[]) throws CloneNotSupportedException {

		Developers devs = new Developers();
		devs.setNameList(loadNameData());
		devs.setPreferredLanguages(loadLanguageData());

		Developers devs_front = (Developers) devs.clone();
		devs_front.getPreferredLanguages().remove("C++");
		devs_front.getNameList().remove("David");

		System.out.println(devs);
		System.out.println("\n" + devs_front);

	}

	// Assume that thess method loads all data from database.
	public static List<String> loadNameData() {
		List<String> namesFromDatabase = new LinkedList<>();

		namesFromDatabase.add("Pankaj");
		namesFromDatabase.add("Raj");
		namesFromDatabase.add("David");
		namesFromDatabase.add("Lisa");

		return namesFromDatabase;
	}

	public static List<String> loadLanguageData() {
		List<String> languagesFromDatabase = new LinkedList<>();

		languagesFromDatabase.add("Java");
		languagesFromDatabase.add("C#");
		languagesFromDatabase.add("C++");
		languagesFromDatabase.add("Python");

		return languagesFromDatabase;
	}
}
